﻿using GEH.Core.Model;
using System.Collections.Generic;

namespace GEH.Core.IService
{
    public interface IStudentService
    {
        IList<Student> GetAllStudent();
    }
}

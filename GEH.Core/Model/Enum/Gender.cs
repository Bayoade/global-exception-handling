﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GEH.Core.Model.Enum
{
    public enum Gender
    {
        Male = 100,
        Female = 101
    }
}

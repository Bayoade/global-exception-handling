﻿using GEH.Core.Model.Enum;
using System;

namespace GEH.Core.Model
{
    public class Student
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int Age { get; set; }

        public Gender Gender { get; set; }
    }
}

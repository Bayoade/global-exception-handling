﻿using GEH.Core.Model;
using System.Collections.Generic;

namespace GEH.Core.IRepository
{
    public interface IStudentRepository
    {
        IList<Student> GetAllStudent();
    }
}

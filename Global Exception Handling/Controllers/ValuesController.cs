﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GEH.Core.IService;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Global_Exception_Handling.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private ILogger _logger;
        private readonly IStudentService _studentService;

        public ValuesController(
            ILogger<ValuesController> logger,
            IStudentService studentService)
        {
            _logger = logger;
            _studentService = studentService;
        }

        //[HttpGet]
        //public IActionResult Get()
        //{
        //    try
        //    {
        //        _logger.LogInformation("Fetching all the Students from the storage");

        //        var students = _studentService.GetAllStudent();

        //        _logger.LogInformation($"Returning {students.Count} students.");

        //        return Ok();
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError($"Something went wrong: {ex}");
        //        return StatusCode(500, "Internal server error");
        //    }
        //}

        public IActionResult Get()
        {
            _logger.LogInformation("Fetching all the Students from the storage");

            var students = _studentService.GetAllStudent();

            throw new Exception("Exception while fetching all the students from the storage.");

            //_logger.LogInformation($"Returning {students.Count} students.");

            //return Ok(students);
        }
    }
}

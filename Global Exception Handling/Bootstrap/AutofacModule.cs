﻿using Autofac;
using GEH.Core.IRepository;
using GEH.Core.IService;
using GEH.Data.Repository;
using GEH.Data.Service;
using Microsoft.Extensions.Logging;

namespace Global_Exception_Handling.Bootstrap
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<StudentRepository>()
                .As<IStudentRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<StudentService>()
                .As<IStudentService>()
                .InstancePerLifetimeScope();
        }
    }
}

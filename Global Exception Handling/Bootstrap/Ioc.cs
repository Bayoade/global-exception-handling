﻿using Microsoft.Extensions.Configuration;

namespace Global_Exception_Handling.Bootstrap
{
    public static class Ioc
    {
    }

    public static class IocContainer
    {
        public static IConfiguration Configuration { get; set; }
    }
}

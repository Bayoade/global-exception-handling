﻿using GEH.Core.IRepository;
using GEH.Core.IService;
using GEH.Core.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace GEH.Data.Service
{
    public class StudentService : IStudentService
    {
        private readonly IStudentRepository _studentRepository;

        public StudentService(IStudentRepository studentRepository)
        {
            _studentRepository = studentRepository;
        }
        public IList<Student> GetAllStudent()
        {
            return _studentRepository.GetAllStudent();
        }
    }
}

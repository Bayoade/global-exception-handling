﻿using GEH.Core.Model;
using GEH.Core.Model.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace GEH.Data.MockData
{
    public static class StudentMockData
    {
        public static IList<Student> AllStudent() => new List<Student>
        {
            new Student
            {
                Id = Guid.NewGuid(),
                FirstName = "Abdulie",
                LastName = "Akinloye",
                Gender = Gender.Male,
                Age = 17
            },
            new Student
            {
                Id = Guid.NewGuid(),
                FirstName = "Adebayomi",
                LastName = "Farinde",
                Gender = Gender.Male,
                Age = 16
            },
            new Student
            {
                Id = Guid.NewGuid(),
                FirstName = "Bilikisu",
                LastName = "Lasisi",
                Gender = Gender.Female,
                Age = 12
            },
            new Student
            {
                Id = Guid.NewGuid(),
                FirstName = "Laide",
                LastName = "Marcus",
                Gender = Gender.Female,
                Age = 14
            }

        };
    }
}

﻿using GEH.Core.IRepository;
using GEH.Core.Model;
using GEH.Data.MockData;
using System;
using System.Collections.Generic;
using System.Text;

namespace GEH.Data.Repository
{
    public class StudentRepository : IStudentRepository
    {
        public IList<Student> GetAllStudent()
        {
            return StudentMockData.AllStudent();
        }
    }
}
